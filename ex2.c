/*
** ex2.c for PSU_2013_my_select in /home/de-dum_m/code/B1-Systeme_Unix/PSU_2013_my_select
**
** Made by Maxime
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Jan 11 16:44:20 2014 Maxime
** Last update Sat Jan 11 19:58:16 2014 Maxime
*/

#include <curses.h>
#include <term.h>
#include <unistd.h>

void	my_putstr(char *str);

int	my_putchar(int c)
{
  write(1, &c, 1);
  return (1);
}

int	my_strlen(char *str)
{
  int	index;

  index = 0;
  while (str && str[index])
    index++;
  return (index);
}

int	main(int ac, char **av)
{
  char	*param;
  struct termios	tp;
  int	i = 0;

  if (tcgetattr(0, &tp) == -1)
    return (-1);
  tp.c_lflag &= ~ECHO;
  tp.c_lflag &= ~ICANON;
  tp.c_cc[VMIN] = 1;
  tp.c_cc[VTIME] = 0;
  tcsetattr(0, 0, &tp);
  if (tgetent(NULL, "xterm") <= 0)
    return (-1);
  tputs(tgetstr("cl", NULL), 1, &my_putchar);
  while (42)
    {
      param = tgoto(tgetstr("cm", NULL), 0, 0);
      tputs(param, 1, &my_putchar);
      my_put_nbr(i++);
    }
}
