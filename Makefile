##
## Makefile for PSU_2013_my_select in /home/de-dum_m/code/B1-Systeme_Unix/PSU_2013_my_select
## 
## Made by Maxime
## Login   <de-dum_m@epitech.net>
## 
## Started on  Sat Jan 11 15:24:07 2014 Maxime
## Last update Sat Jan 11 19:51:02 2014 Maxime
##

NAME	= my_select

SRC	= ex1.c ex2.c my_put_nbr.c

OBJ	= $(SRC:.c=.o)

CFLAGS	= -Wall -Wextra -W -pedantic

$NAME:	$(OBJ)
	$(CC) $(OBJ) -o $(NAME) -lncurses
	@echo -e "[032mCompiled successfully[0m"

all:	$(NAME)

clean:
	rm -f $(OBJ)

fclean:	clean
	rm -f $(NAME)

re:	fclean all

.PHONY:	all re fclean re
