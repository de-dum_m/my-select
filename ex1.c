/*
** ex1.c for PSU_2013_my_select in /home/armita_a/Documents/Teck_1/System_unix/PSU_2013_my_select
**
** Made by
** Login   <armita_a@epitech.net>
**
** Started on  Sat Jan 11 15:48:03 2014
** Last update Sat Jan 11 16:47:26 2014 Maxime
*/


#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define SIZE 2046

int			my_get()
{
  struct termios	tp;

  if (tcgetattr(0, &tp) == -1)
    return (-1);
  tp.c_lflag &= ~ECHO;
  tp.c_lflag &= ~ICANON;
  tp.c_cc[VMIN] = 1;
  tp.c_cc[VTIME] = 0;
  tcsetattr(0, 0, &tp);
}

void	my_putstr(char *str)
{
  int	index;

  index = 0;
  while (str[index])
    write(1, &str[index++], 1);
}

/* int	main() */
/* { */
/*   int	i; */
/*   int	red; */
/*   char	c[SIZE]; */

/*   i = 0; */
/*   my_get(); */
/*   my_putstr("Please enter your password: "); */
/*   while ((i == 0 || c[i - 1] != '\n')) */
/*     { */
/*       read(0, &c[i], 1); */
/*       if (c[i++] != '\n') */
/* 	my_putchar('*', 1); */
/*     } */
/*   c[--i] = 0; */
/*   my_putstr("\nYour password: "); */
/*   my_putstr(c); */
/*   my_putchar('\n', 1); */
/* } */
