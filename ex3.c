/*
** ex2.c for PSU_2013_my_select in /home/de-dum_m/code/B1-Systeme_Unix/PSU_2013_my_select
**
** Made by Maxime
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Jan 11 16:44:20 2014 Maxime
** Last update Sat Jan 11 17:44:54 2014 
*/

#include <curses.h>
#include <term.h>
#include <unistd.h>

int	my_strlen(char *str)
{
  int	index;

  index = 0;
  while (str[index])
    index++;
  return (index);
}

int	main(int argc, char **argv)
{
  char	*param;
  struct termios	tp;

  if (!argv[1])
    return (-1);
  if (tcgetattr(0, &tp) == -1)
    return (-1);
  tp.c_lflag &= ~ECHO;
  tp.c_lflag &= ~ICANON;
  tp.c_cc[VMIN] = 1;
  tp.c_cc[VTIME] = 0;
  tcsetattr(0, 0, &tp);
  if (tgetent(NULL, "xterm") <= 0)
    return (-1);
  param = tgetstr("mr", NULL);
  write(1, param, my_strlen(param));
  my_putstr(argv[1]);
}
