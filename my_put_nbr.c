/*
** my_put_nbr.c for Mini_lib.c in /home/armita_a/Documents/Teck_1/mini_lib
**
** Made by armita_a
** Login   <armita_a@epitech.net>
**
** Started on  Tue Jan  7 18:12:09 2014 armita_a
** Last update Sat Jan 11 19:36:53 2014 Maxime
*/

#include <unistd.h>

static void	my_putchar(char c, int fd)
{
  write(fd, &c, 1);
}

int	my_put_nbr(int nb)
{
  if (nb < 0)
    {
      nb = - nb;
      my_putchar('-', 1);
    }
  if (nb >= 10)
    {
      my_put_nbr(nb/10);
      my_put_nbr(nb%10);
    }
  else
    my_putchar(nb + 48, 1);
  return (0);
}
