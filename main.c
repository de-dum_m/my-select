/*
** main.c for PSU_2013_my_select in /home/de-dum_m/code/B1-Systeme_Unix/PSU_2013_my_select
**
** Made by Maxime
** Login   <de-dum_m@epitech.net>
**
** Started on  Sat Jan 11 15:27:46 2014 Maxime
** Last update Sat Jan 11 15:46:56 2014 Maxime
*/

int	main(int av, int ac)
{
  if (ac <= 1)
    return (usage());
  start_all(ac, av);
  return (SUCCESS);
}
